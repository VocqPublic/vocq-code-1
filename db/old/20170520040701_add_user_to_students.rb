class AddUserToStudents < ActiveRecord::Migration[5.0]
  def change
    add_reference(:students, :user, index: true, null: false)
  end
end
