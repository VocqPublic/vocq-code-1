class RemoveSchoolFromStudents < ActiveRecord::Migration[5.0]
  def change
    remove_reference(:students, :school, index: true)
  end
end
