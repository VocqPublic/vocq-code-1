class CreateFeedbackForms < ActiveRecord::Migration[5.0]
  def change
    create_table :feedback_forms do |t|
      t.text :slug, null: false, unique: true
      t.text :question1, null: false
      t.text :question2, null: false
      t.text :question3, null: false
      t.references :school, foreign_key: true

      t.timestamps
    end
  end
end
