class CreateStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :students do |t|
      t.references :school, foreign_key: true, null: false
      t.string :student_id, null: false

      t.timestamps
    end
    add_index :students, [:school_id, :student_id], unique: true
  end
end
