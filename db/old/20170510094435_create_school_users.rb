class CreateSchoolUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :school_users do |t|
      t.references :school, foreign_key: true, null: false
      t.references :user, foreign_key: true, null: false

      t.timestamps
    end
    add_index :school_users, [:school_id, :user_id], unique: true
  end
end
