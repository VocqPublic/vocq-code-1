class CreateFeedbackEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :feedback_entries do |t|
      t.references :feedback_form, foreign_key: true, null: false
      t.references :student, foreign_key: true, null: false
      t.text :week, null: false
      t.integer :score, null: false
      t.text :question2, null: false
      t.text :question3, null: false

      t.text :question2_topic
      t.text :question3_topic

      t.timestamps
    end
  end
end
