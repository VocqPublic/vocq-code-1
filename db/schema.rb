# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170725062625) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "chat_messages", force: :cascade do |t|
    t.integer  "message_type",         null: false
    t.text     "content",              null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "edu_class_student_id", null: false
    t.index ["edu_class_student_id"], name: "index_chat_messages_on_edu_class_student_id", using: :btree
  end

  create_table "edu_class_students", force: :cascade do |t|
    t.integer  "edu_class_id",   null: false
    t.integer  "student_id",     null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "name",           null: false
    t.string   "student_number", null: false
    t.index ["edu_class_id", "student_id"], name: "index_edu_class_students_on_edu_class_id_and_student_id", unique: true, using: :btree
    t.index ["edu_class_id"], name: "index_edu_class_students_on_edu_class_id", using: :btree
    t.index ["student_id"], name: "index_edu_class_students_on_student_id", using: :btree
  end

  create_table "edu_classes", force: :cascade do |t|
    t.string   "name",             null: false
    t.integer  "teacher_id",       null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "inbound_email_id", null: false
    t.index ["name", "teacher_id"], name: "index_edu_classes_on_name_and_teacher_id", unique: true, using: :btree
    t.index ["teacher_id"], name: "index_edu_classes_on_teacher_id", using: :btree
  end

  create_table "feedback_entries", force: :cascade do |t|
    t.integer  "feedback_form_id", null: false
    t.integer  "student_id",       null: false
    t.integer  "question_score1",  null: false
    t.text     "question2",        null: false
    t.text     "question3"
    t.text     "question2_topic"
    t.text     "question3_topic"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "date_start",       null: false
    t.string   "question4"
    t.string   "question5"
    t.integer  "question_score3"
    t.integer  "question_score4"
    t.integer  "question_score5"
    t.integer  "question_score2"
    t.string   "question1"
    t.index ["date_start"], name: "index_feedback_entries_on_date_start", using: :btree
    t.index ["feedback_form_id"], name: "index_feedback_entries_on_feedback_form_id", using: :btree
    t.index ["student_id"], name: "index_feedback_entries_on_student_id", using: :btree
  end

  create_table "feedback_forms", force: :cascade do |t|
    t.integer  "edu_class_id",                                null: false
    t.text     "slug",                                        null: false
    t.text     "question1",                                   null: false
    t.text     "question2",                                   null: false
    t.text     "question3"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "question4"
    t.string   "question5"
    t.string   "title",                   default: "Form",    null: false
    t.string   "color_background",        default: "#83266F", null: false
    t.string   "color_foreground",        default: "#FFFFFF", null: false
    t.string   "color_question",          default: "#000000", null: false
    t.string   "color_help_text",         default: "#686868", null: false
    t.string   "color_button_background", default: "#3093c7", null: false
    t.string   "color_button_text",       default: "#efefef", null: false
    t.integer  "question_type1",          default: 1,         null: false
    t.integer  "question_type2",          default: 2,         null: false
    t.integer  "question_type3",          default: 1,         null: false
    t.integer  "question_type4",          default: 1,         null: false
    t.integer  "question_type5",          default: 1,         null: false
    t.string   "question1_data"
    t.string   "question2_data"
    t.string   "question3_data"
    t.string   "question4_data"
    t.string   "question5_data"
    t.boolean  "is_published",            default: false,     null: false
    t.index ["edu_class_id"], name: "index_feedback_forms_on_edu_class_id", using: :btree
  end

  create_table "sendgrid_inbound_data", force: :cascade do |t|
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "to"
    t.string   "from"
    t.string   "cc"
    t.string   "subject"
    t.text     "body"
    t.text     "raw_text"
    t.text     "raw_html"
    t.text     "raw_body"
    t.text     "attachments"
    t.text     "headers"
    t.text     "raw_headers"
    t.string   "ip_address"
  end

  create_table "students", force: :cascade do |t|
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name",                                null: false
    t.string   "email",                               null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.index ["email"], name: "index_students_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_students_on_reset_password_token", unique: true, using: :btree
  end

  create_table "teachers", force: :cascade do |t|
    t.string   "email",                  default: "",        null: false
    t.string   "encrypted_password",     default: "",        null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,         null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "onboard_status",         default: 0
    t.string   "name",                   default: "Teacher", null: false
    t.string   "designation",            default: "Teacher", null: false
    t.index ["email"], name: "index_teachers_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_teachers_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "chat_messages", "edu_class_students"
  add_foreign_key "edu_class_students", "edu_classes"
  add_foreign_key "edu_class_students", "students"
  add_foreign_key "edu_classes", "teachers"
  add_foreign_key "feedback_entries", "feedback_forms"
  add_foreign_key "feedback_entries", "students"
  add_foreign_key "feedback_forms", "edu_classes"
end
