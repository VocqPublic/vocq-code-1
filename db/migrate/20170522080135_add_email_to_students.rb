class AddEmailToStudents < ActiveRecord::Migration[5.0]
  def change
    add_column :students, :email, :string, null: false
    add_index :students, [:teacher_id, :email], unique: true
  end
end
