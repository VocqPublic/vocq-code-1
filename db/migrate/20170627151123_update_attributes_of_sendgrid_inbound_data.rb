class UpdateAttributesOfSendgridInboundData < ActiveRecord::Migration[5.0]
  def change
    add_column :sendgrid_inbound_data, :to, :string
    add_column :sendgrid_inbound_data, :from, :string
    add_column :sendgrid_inbound_data, :cc, :string
    add_column :sendgrid_inbound_data, :subject, :string

    add_column :sendgrid_inbound_data, :body, :text
    add_column :sendgrid_inbound_data, :raw_text, :text
    add_column :sendgrid_inbound_data, :raw_html, :text
    add_column :sendgrid_inbound_data, :raw_body, :text
    add_column :sendgrid_inbound_data, :attachments, :text
    add_column :sendgrid_inbound_data, :headers, :text
    add_column :sendgrid_inbound_data, :raw_headers, :text

    remove_column :sendgrid_inbound_data, :payload_raw, :text
    remove_column :sendgrid_inbound_data, :payload_headers, :text
    remove_column :sendgrid_inbound_data, :payload_body
    remove_column :sendgrid_inbound_data, :ip_address, :text

    add_column :sendgrid_inbound_data, :ip_address, :string
  end
end
