class CreateEduClasses < ActiveRecord::Migration[5.0]
  def change
    create_table :edu_classes do |t|
      t.string :name, null: false
      t.references :teacher, foreign_key: true, null: false

      t.timestamps
    end

    add_index :edu_classes, [:name, :teacher_id], unique: true
  end
end
