class AddIsPublishedToFeedbackForms < ActiveRecord::Migration[5.0]
  def change
    add_column :feedback_forms, :is_published, :boolean, default: false, null: false

    FeedbackForm.all.each do |feedback_form|
      if !feedback_form.expires_on.nil? && feedback_form.expires_on > Time.zone.now
        feedback_form.is_published = true
        feedback_form.save
      end
    end
  end
end
