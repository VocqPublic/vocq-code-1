class AddMoreQuestionScoreToFeedbackEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :feedback_entries, :question_score2, :integer
    rename_column :feedback_entries, :question1, :question_score1
    add_column :feedback_entries, :question1, :string
  end
end
