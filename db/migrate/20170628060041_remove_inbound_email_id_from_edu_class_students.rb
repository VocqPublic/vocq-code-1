class RemoveInboundEmailIdFromEduClassStudents < ActiveRecord::Migration[5.0]
  def change
    remove_column :edu_class_students, :inbound_email_id, :string
  end
end
