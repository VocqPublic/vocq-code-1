class RenamePayloadToPayloadRawForSendgridInboundData < ActiveRecord::Migration[5.0]
  def change
    rename_column :sendgrid_inbound_data, :payload, :payload_raw
  end
end
