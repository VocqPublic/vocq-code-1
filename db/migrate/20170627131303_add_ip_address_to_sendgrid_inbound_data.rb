class AddIpAddressToSendgridInboundData < ActiveRecord::Migration[5.0]
  def change
    add_column :sendgrid_inbound_data, :ip_address, :text
  end
end
