class AddSubjectToEduClass < ActiveRecord::Migration[5.0]
  def change
    add_column :edu_classes, :subject, :string, null: false
  end
end
