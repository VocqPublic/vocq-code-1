class AddDeviseToStudents < ActiveRecord::Migration[5.0]
  def change
    add_column :students, :encrypted_password, :string, null: false, default: ""
    add_column :students, :reset_password_token, :string
    add_column :students, :reset_password_sent_at, :datetime
    add_column :students, :remember_created_at, :datetime
    add_column :students, :sign_in_count, :integer, default: 0, null: false
    add_column :students, :current_sign_in_at, :datetime
    add_column :students, :last_sign_in_at, :datetime
    add_column :students, :current_sign_in_ip, :inet
    add_column :students, :last_sign_in_ip, :inet

    add_index :students, :email,                unique: true
    add_index :students, :reset_password_token, unique: true
  end
end
