class AddEduClassStudentToChatMessages < ActiveRecord::Migration[5.0]
  def change
    add_reference :chat_messages, :edu_class_student, null: false, index: true, foreign_key: true
  end
end
