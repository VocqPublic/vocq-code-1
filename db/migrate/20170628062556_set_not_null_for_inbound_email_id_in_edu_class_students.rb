class SetNotNullForInboundEmailIdInEduClassStudents < ActiveRecord::Migration[5.0]
  def change
    change_column_null :edu_classes, :inbound_email_id, false
  end
end
