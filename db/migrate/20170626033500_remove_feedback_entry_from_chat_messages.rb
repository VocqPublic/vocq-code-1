class RemoveFeedbackEntryFromChatMessages < ActiveRecord::Migration[5.0]
  def change
    remove_column :chat_messages, :feedback_entry_id
  end
end
