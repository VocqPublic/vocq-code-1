class AddColorsToFeedbackForms < ActiveRecord::Migration[5.0]
  def change
    add_column :feedback_forms, :color_background, :string, null: false, default: '#83266F'
    add_column :feedback_forms, :color_foreground, :string, null: false, default: '#FFFFFF'
    add_column :feedback_forms, :color_question, :string, null: false, default: '#000000'
    add_column :feedback_forms, :color_help_text, :string, null: false, default: '#686868'
    add_column :feedback_forms, :color_button_background, :string, null: false, default: '#3093c7'
    add_column :feedback_forms, :color_button_text, :string, null: false, default: '#efefef'
  end
end
