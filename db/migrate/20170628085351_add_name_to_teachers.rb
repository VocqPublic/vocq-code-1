class AddNameToTeachers < ActiveRecord::Migration[5.0]
  def change
    add_column :teachers, :name, :string, default: 'Teacher', null: false
  end
end
