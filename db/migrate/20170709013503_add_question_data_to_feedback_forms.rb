class AddQuestionDataToFeedbackForms < ActiveRecord::Migration[5.0]
  def change
    add_column :feedback_forms, :question1_data, :string
    add_column :feedback_forms, :question2_data, :string
    add_column :feedback_forms, :question3_data, :string
    add_column :feedback_forms, :question4_data, :string
    add_column :feedback_forms, :question5_data, :string
  end
end
