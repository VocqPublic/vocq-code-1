class AddPayloadHeadersToSendgridInboundData < ActiveRecord::Migration[5.0]
  def change
    add_column :sendgrid_inbound_data, :payload_headers, :text
  end
end
