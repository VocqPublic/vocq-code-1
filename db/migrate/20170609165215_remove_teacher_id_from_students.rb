class RemoveTeacherIdFromStudents < ActiveRecord::Migration[5.0]
  def change
    remove_index :students, [:teacher_id, :student_id]
    remove_column :students, :teacher_id
  end
end
