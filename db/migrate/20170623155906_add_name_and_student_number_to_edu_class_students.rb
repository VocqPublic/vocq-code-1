class AddNameAndStudentNumberToEduClassStudents < ActiveRecord::Migration[5.0]
  def change
    add_column :edu_class_students, :name, :string
    add_column :edu_class_students, :student_number, :string

    EduClassStudent.all.each do |edu_class_student|
      edu_class_student.name = edu_class_student.student.name
      edu_class_student.student_number = edu_class_student.student.student_id
      edu_class_student.save
    end

    change_column_null :edu_class_students, :name, false
    change_column_null :edu_class_students, :student_number, false
  end
end
