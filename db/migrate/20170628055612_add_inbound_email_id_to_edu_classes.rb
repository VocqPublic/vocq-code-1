class AddInboundEmailIdToEduClasses < ActiveRecord::Migration[5.0]
  def change
    add_column :edu_classes, :inbound_email_id, :string, index: true

    EduClass.all.each do |edu_class|
      edu_class.inbound_email_id = SecureRandom.hex + 'c' + edu_class.id.to_i.to_s(16)
      edu_class.save
    end
  end
end
