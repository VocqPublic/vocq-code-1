class AddNameToStudents < ActiveRecord::Migration[5.0]
  def change
    add_column :students, :name, :string, null: false
    add_index :students, [:teacher_id, :name], unique: true
  end
end
