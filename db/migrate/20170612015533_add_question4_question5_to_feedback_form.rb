class AddQuestion4Question5ToFeedbackForm < ActiveRecord::Migration[5.0]
  def change
    change_column_null :feedback_forms, :question3, true
    add_column :feedback_forms, :question4, :string
    add_column :feedback_forms, :question5, :string
  end
end
