class AddExpiresOnToFeedbackForms < ActiveRecord::Migration[5.0]
  def change
    add_column :feedback_forms, :expires_on, :datetime
  end
end
