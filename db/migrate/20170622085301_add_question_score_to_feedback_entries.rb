class AddQuestionScoreToFeedbackEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :feedback_entries, :question_score3, :integer
    add_column :feedback_entries, :question_score4, :integer
    add_column :feedback_entries, :question_score5, :integer
  end
end
