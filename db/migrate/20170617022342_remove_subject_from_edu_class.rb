class RemoveSubjectFromEduClass < ActiveRecord::Migration[5.0]
  def change
    remove_column :edu_classes, :subject
  end
end
