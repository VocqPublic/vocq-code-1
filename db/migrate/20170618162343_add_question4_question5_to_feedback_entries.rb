class AddQuestion4Question5ToFeedbackEntries < ActiveRecord::Migration[5.0]
  def change
    change_column_null :feedback_entries, :question3, true
    add_column :feedback_entries, :question4, :string
    add_column :feedback_entries, :question5, :string
  end
end
