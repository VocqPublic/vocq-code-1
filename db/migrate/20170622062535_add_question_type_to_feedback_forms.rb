class AddQuestionTypeToFeedbackForms < ActiveRecord::Migration[5.0]
  def change
    add_column :feedback_forms, :question_type1, :integer, default: 1, null: false
    add_column :feedback_forms, :question_type2, :integer, default: 2, null: false
    add_column :feedback_forms, :question_type3, :integer, default: 1, null: false
    add_column :feedback_forms, :question_type4, :integer, default: 1, null: false
    add_column :feedback_forms, :question_type5, :integer, default: 1, null: false
  end
end
