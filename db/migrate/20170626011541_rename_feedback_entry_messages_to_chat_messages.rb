class RenameFeedbackEntryMessagesToChatMessages < ActiveRecord::Migration[5.0]
  def change
    rename_table :feedback_entry_messages, :chat_messages
  end
end
