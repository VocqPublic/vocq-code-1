class AddDesignationToTeachers < ActiveRecord::Migration[5.0]
  def change
    add_column :teachers, :designation, :string, default: 'Teacher', null: false
  end
end
