class RemoveStudentIdFromStudents < ActiveRecord::Migration[5.0]
  def change
    remove_column :students, :student_id
  end
end
