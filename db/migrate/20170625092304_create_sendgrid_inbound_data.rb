class CreateSendgridInboundData < ActiveRecord::Migration[5.0]
  def change
    create_table :sendgrid_inbound_data do |t|
      t.text :payload, null: false

      t.timestamps
    end
  end
end
