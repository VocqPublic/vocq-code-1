class AddOnboardStatusToTeachers < ActiveRecord::Migration[5.0]
  def change
    add_column :teachers, :onboard_status, :integer, default: 0
  end
end
