class AddDateStartToFeedbackEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :feedback_entries, :date_start, :string
    add_index :feedback_entries, :date_start

    FeedbackEntry.find_each do |fb|
      fb.date_start = fb.created_at.beginning_of_day.strftime(FeedbackEntry::DATE_START_FORMAT)
      fb.save
    end

    change_column_null :feedback_entries, :date_start, false
  end
end
