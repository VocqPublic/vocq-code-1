class RenameTypeToMessageTypeInChatMessages < ActiveRecord::Migration[5.0]
  def change
    rename_column :chat_messages, :type, :message_type
  end
end
