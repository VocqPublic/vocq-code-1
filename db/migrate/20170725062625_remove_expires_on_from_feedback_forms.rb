class RemoveExpiresOnFromFeedbackForms < ActiveRecord::Migration[5.0]
  def change
    remove_column :feedback_forms, :expires_on
  end
end
