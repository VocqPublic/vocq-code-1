class CreateEduClassStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :edu_class_students do |t|
      t.references :edu_class, foreign_key: true, null: false
      t.references :student, foreign_key: true, null: false

      t.timestamps
    end
    add_index :edu_class_students, [:edu_class_id, :student_id], unique: true
  end
end
