class SetAllStudentsPasswordToTheirEmail < ActiveRecord::Migration[5.0]
  def change
    Student.all.each do |student|
      student.password = student.email
      student.save
    end
  end
end
