class AddTitleToFeedbackForms < ActiveRecord::Migration[5.0]
  def change
    add_column :feedback_forms, :title, :string, null: false, default: 'Form'
  end
end
