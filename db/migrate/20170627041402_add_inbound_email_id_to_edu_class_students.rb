class AddInboundEmailIdToEduClassStudents < ActiveRecord::Migration[5.0]
  def change
    add_column :edu_class_students, :inbound_email_id, :string, index: true

    EduClassStudent.all.each do |edu_class_student|
      edu_class_student.inbound_email_id = SecureRandom.hex + 's' + edu_class_student.id.to_i.to_s(16)
      edu_class_student.save
    end

    change_column_null :edu_class_students, :inbound_email_id, false
  end
end
