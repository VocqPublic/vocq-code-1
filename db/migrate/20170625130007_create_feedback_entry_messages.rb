class CreateFeedbackEntryMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :feedback_entry_messages do |t|
      t.references :feedback_entry, foreign_key: true, null: false
      t.integer :type, null: false
      t.text :content, null: false

      t.timestamps
    end
  end
end
