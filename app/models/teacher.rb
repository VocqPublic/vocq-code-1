class Teacher < ApplicationRecord
  ONBOARD_STATUS_NO_CLASS    = 0
  ONBOARD_STATUS_NO_STUDENTS = 1
  ONBOARD_STATUS_OK          = 2

  ERROR_CREATE_A_CLASS_FIRST = 'create_a_class_first'
  ERROR_ADD_STUDENTS_FIRST = 'add_students_first'

  devise :database_authenticatable, :registerable,
         :recoverable,
         :trackable, :validatable

  has_many :edu_classes
  validates :email, uniqueness: true, presence: true, length: { maximum: 200 }

  def latest_onboard_status
    unless self.onboard_status == ONBOARD_STATUS_OK
      if self.onboard_status == ONBOARD_STATUS_NO_CLASS
        if edu_classes.count > 0
          self.onboard_status = self.onboard_status + 1
          save
        end
      end
      if onboard_status == ONBOARD_STATUS_NO_STUDENTS
        edu_class = edu_classes.first
        if edu_class.students.count > 0
          self.onboard_status = self.onboard_status + 1
          save
        end
      end
    end
    self.onboard_status
  end

  def onboard_error_message
    case onboard_status
    when ONBOARD_STATUS_NO_CLASS 
      ERROR_CREATE_A_CLASS_FIRST
    when ONBOARD_STATUS_NO_STUDENTS 
      ERROR_ADD_STUDENTS_FIRST
    end
  end
end
