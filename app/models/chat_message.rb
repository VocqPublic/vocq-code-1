class ChatMessage < ApplicationRecord
  MESSAGE_TYPE_STUDENT  = FIRST_TYPE = 1
  MESSAGE_TYPE_TEACHER  = 2
  MESSAGE_TYPE_QUESTION = LAST_TYPE = 3
  
  belongs_to :edu_class_student

  validates_presence_of :message_type
  validates_presence_of :content

  validates :message_type, numericality: { only_integer: true, greater_than_or_equal_to: FIRST_TYPE, less_than_or_equal_to: LAST_TYPE }
  validates :content, length: { maximum: 10000 }

  after_create_commit :send_email

  private
  def send_email
    if self.message_type == MESSAGE_TYPE_TEACHER
      reply_to_email = self.edu_class_student.edu_class.inbound_email_id + '@test.vocq-mail.com'
      teacher_name = 'Teacher'
      student_email = self.edu_class_student.student.email
      student_name = self.edu_class_student.name

      ApplicationHelper::send_email(reply_to_email, teacher_name,
                                    student_email, student_name,
                                    'About Feedback', self.content)
    end
  end
end
