class FeedbackEntry < ApplicationRecord

  DATE_FORMAT = '%d-%m-%Y'
  DATE_START_FORMAT = DATE_FORMAT

  belongs_to :feedback_form
  belongs_to :student

  validates_presence_of :date_start
  validates_presence_of :question_score1
  validates_presence_of :question2

  validates :question_score1, numericality: { only_integer: true }
  validates :question_score2, numericality: { only_integer: true }, allow_blank: true
  validates :question_score3, numericality: { only_integer: true }, allow_blank: true
  validates :question_score4, numericality: { only_integer: true }, allow_blank: true
  validates :question_score5, numericality: { only_integer: true }, allow_blank: true

  validates :question1, length: { maximum: 500 }
  validates :question2, length: { maximum: 500 }
  validates :question3, length: { maximum: 500 }, allow_blank: true
  validates :question4, length: { maximum: 500 }, allow_blank: true
  validates :question5, length: { maximum: 500 }, allow_blank: true

  validates_uniqueness_of :student, scope: [:feedback_form, :date_start] # test this

  after_initialize :set_date_start
  after_create_commit :create_messages

  private
  def set_date_start
    #week ||= 'Y' + Time.current.year.to_s + 'W' + (Time.now.strftime('%U').to_i + 1).to_s
    self.date_start ||= Time.zone.now.strftime(DATE_START_FORMAT)
  end

  def create_messages
    edu_class_student = EduClassStudent.where(edu_class_id: self.feedback_form.edu_class.id, student_id: self.student.id).first

    unless self.feedback_form.question1.blank?
      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_QUESTION,
                           content: self.feedback_form.question1,
                           created_at: self.created_at,
                           updated_at: self.updated_at})
      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_STUDENT,
                           content: self.question1,
                           created_at: self.created_at,
                           updated_at: self.updated_at})
    end
    unless self.feedback_form.question2.blank?
      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_QUESTION,
                           content: self.feedback_form.question2,
                           created_at: self.created_at,
                           updated_at: self.updated_at})
      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_STUDENT,
                           content: self.question2,
                           created_at: self.created_at,
                           updated_at: self.updated_at})
    end
    unless self.feedback_form.question3.blank?
      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_QUESTION,
                           content: self.feedback_form.question3,
                           created_at: self.created_at,
                           updated_at: self.updated_at})
      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_STUDENT,
                           content: self.question3,
                           created_at: self.created_at,
                           updated_at: self.updated_at})
    end
    unless self.feedback_form.question4.blank?
      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_QUESTION,
                           content: self.feedback_form.question4,
                           created_at: self.created_at,
                           updated_at: self.updated_at})
      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_STUDENT,
                           content: self.question4,
                           created_at: self.created_at,
                           updated_at: self.updated_at})
    end
    unless self.feedback_form.question5.blank?
      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_QUESTION,
                           content: self.feedback_form.question5,
                           created_at: self.created_at,
                           updated_at: self.updated_at})
      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_STUDENT,
                           content: self.question5,
                           created_at: self.created_at,
                           updated_at: self.updated_at})
    end
  end
end
