include Rails.application.routes.url_helpers

class FeedbackForm < ApplicationRecord
  QUESTION_TYPE_RATING = 1
  QUESTION_TYPE_TEXT  = 2
  QUESTION_TYPE_MULTIPLE_CHOICE = 3

  FIRST_QUESTION_TYPE = 1
  LAST_QUESTION_TYPE = 3

  QUESTION_NAME_RATING = 'Rating'
  QUESTION_NAME_TEXT = 'Text'
  QUESTION_NAME_MULTIPLE_CHOICE = 'Multiple Choice'

  COLOR_REGEX = /\A#(\h{6}|\h{3})\z/

  DEFAULT_QUESTION_1 = 'On a scale of 1-5, how would you rate your learning experience?'
  DEFAULT_QUESTION_2 = 'What is the main reason for the score you gave on your learning experience?'
  DEFAULT_QUESTION_1_TYPE = QUESTION_TYPE_RATING
  DEFAULT_QUESTION_2_TYPE = QUESTION_TYPE_TEXT

  belongs_to :edu_class
  has_many :feedback_entries, dependent: :destroy

  validates :title, presence: true, length: { maximum: 50 }
  validates :slug, presence: true, length: { maximum: 50 }
  
  validates_presence_of :question1
  validates_presence_of :question2
  validates_presence_of :question_type1
  validates_presence_of :question_type1

  validates :question1, length: { maximum: 200 }
  validates :question2, length: { maximum: 200 }
  validates :question3, length: { maximum: 200 }, allow_blank: true
  validates :question4, length: { maximum: 200 }, allow_blank: true
  validates :question5, length: { maximum: 200 }, allow_blank: true

  validates :question_type1, numericality: { only_integer: true, greater_than_or_equal_to: FIRST_QUESTION_TYPE, less_than_or_equal_to: LAST_QUESTION_TYPE }
  validates :question_type2, numericality: { only_integer: true, greater_than_or_equal_to: FIRST_QUESTION_TYPE, less_than_or_equal_to: LAST_QUESTION_TYPE }
  validates :question_type3, numericality: { only_integer: true, greater_than_or_equal_to: FIRST_QUESTION_TYPE, less_than_or_equal_to: LAST_QUESTION_TYPE }, allow_blank: true
  validates :question_type4, numericality: { only_integer: true, greater_than_or_equal_to: FIRST_QUESTION_TYPE, less_than_or_equal_to: LAST_QUESTION_TYPE }, allow_blank: true
  validates :question_type5, numericality: { only_integer: true, greater_than_or_equal_to: FIRST_QUESTION_TYPE, less_than_or_equal_to: LAST_QUESTION_TYPE }, allow_blank: true

  validates :color_background,        presence: true, format: { with: COLOR_REGEX, message: 'not a valid color format.' }
  validates :color_foreground,        presence: true, format: { with: COLOR_REGEX, message: 'not a valid color format.' }
  validates :color_question,          presence: true, format: { with: COLOR_REGEX, message: 'not a valid color format.' }
  validates :color_help_text,         presence: true, format: { with: COLOR_REGEX, message: 'not a valid color format.' }
  validates :color_button_background, presence: true, format: { with: COLOR_REGEX, message: 'not a valid color format.' }
  validates :color_button_text,       presence: true, format: { with: COLOR_REGEX, message: 'not a valid color format.' }

  after_initialize :set_default_values

  def self.question_types
    [
      {
        id:   QUESTION_TYPE_RATING,
        name: QUESTION_NAME_RATING,
      },
      {
        id:   QUESTION_TYPE_TEXT,
        name: QUESTION_NAME_TEXT,
      },
      {
        id:   QUESTION_TYPE_MULTIPLE_CHOICE,
        name: QUESTION_NAME_MULTIPLE_CHOICE,
      },
    ]
  end

  def question_types
    self.class.question_types
  end

  def url
    student_portal_url
  end

  def questions_list 
    arr = [] 
    arr.push(DEFAULT_QUESTION_1) 
    arr.push(DEFAULT_QUESTION_2) 
 
    unless self.question3.blank? 
      arr.push(self.question3) 
    end 
 
    unless self.question4.blank?
      arr.push(self.question4) 
    end 
 
    unless self.question5.blank?
      arr.push(self.question5) 
    end 
 
    arr 
  end 

  def questions
    require 'json'
    arr = []
    arr.push({ text: DEFAULT_QUESTION_1,
               type: DEFAULT_QUESTION_1_TYPE,
               data: { steps: 5 },
               fixed: true })
    arr.push({ text: DEFAULT_QUESTION_2,
               type: DEFAULT_QUESTION_2_TYPE,
               data: nil,
               fixed: true })
    
    unless self.question3.nil?
      arr.push({ text: self.question3, 
                 type: self.question_type3,
                 data: self.question3_data.nil? ? nil : JSON.parse(self.question3_data),
                 fixed: false })
    end
    unless self.question4.nil?
      arr.push({ text: self.question4,
                 type: self.question_type4,
                 data: self.question4_data.nil? ? nil : JSON.parse(self.question4_data),
                 fixed: false })
    end
    unless self.question5.nil?
      arr.push({ text: self.question5,
                 type: self.question_type5,
                 data: self.question5_data.nil? ? nil : JSON.parse(self.question5_data),
                 fixed: false })
    end
    arr
  end

  def update_question(number, text, type, data)
    if number > 2
      case number
      when 3
        self.question3 = text
        self.question_type3 = type
        self.question3_data = data
        save
      when 4
        self.question4 = text
        self.question_type4 = type
        self.question4_data = data
        save
      when 5
        self.question5 = text
        self.question_type5 = type
        self.question5_data = data
        save
      end
    end
  end

  def delete_question(number)
    if number > 2
      case number
      when 3
        self.question3 = self.question4
        self.question4 = self.question5
        self.question5 = nil
        save
      when 4
        self.question4 = self.question5
        self.question5 = nil
        save
      when 5
        self.question5 = nil
        save
      end
    end
  end

  def colors
    {
      background: self.color_background,
      foreground: self.color_foreground,
      question: self.color_question,
      helpText: self.color_help_text,
      buttonBackground: self.color_button_background,
      buttonText: self.color_button_text,
    }
  end

  def publish
    self.is_published = true
    save
  end

  def unpublish
    self.is_published = false
    save
  end

  def replicate_form(form)
    self.title = form.title

    self.question1 = form.question1
    self.question2 = form.question2
    self.question3 = form.question3
    self.question4 = form.question4
    self.question5 = form.question5

    self.question_type1 = form.question_type1
    self.question_type2 = form.question_type2
    self.question_type3 = form.question_type3
    self.question_type4 = form.question_type4
    self.question_type5 = form.question_type5

    self.color_background = form.color_background
    self.color_foreground = form.color_foreground
    self.color_question = form.color_question
    self.color_help_text = form.color_help_text
    self.color_button_background = form.color_button_background
    self.color_button_text = form.color_button_text

    save
  end

  private
  def set_default_values
    require 'securerandom'
    self.slug ||= SecureRandom.hex + self.id.to_i.to_s(16) # need to convert to_i first
    self.question1 ||= DEFAULT_QUESTION_1
    self.question2 ||= DEFAULT_QUESTION_2
    self.question_type1 ||= DEFAULT_QUESTION_1_TYPE
    self.question_type2 ||= DEFAULT_QUESTION_2_TYPE
  end
end
