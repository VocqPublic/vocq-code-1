class EduClass < ApplicationRecord
  belongs_to :teacher
  has_one :feedback_form, dependent: :destroy
  has_many :edu_class_students, dependent: :destroy
  has_many :students, through: :edu_class_students

  validates :name, presence: true, length: { maximum: 20 }

  validates_uniqueness_of :name, scope: :teacher

  before_create :set_inbound_email_id
  after_create_commit :create_feedback_form

  def add_student(email, name, student_number)
    error = nil
    email = email.downcase
    student = Student.where(email: email).first
    if student.nil?
      student = Student.new
      student.email = email
      student.password = email
      student.name = name

      Student.transaction do
        if student.save
          edu_class_student = EduClassStudent.new(student_id: student.id,
                                 edu_class_id: self.id,
                                 name: name,
                                 student_number: student_number)
          unless edu_class_student.save
            error = email + ': ' + edu_class_student.errors.full_messages.join(' ')
          end
        else
          error = student.errors.full_messages.join(' ')
        end
      end
    else
      edu_class_student = EduClassStudent.where(student_id: student.id, edu_class_id: self.id).first
      if edu_class_student.nil?
        edu_class_student = EduClassStudent.new(student_id: student.id,
                               edu_class_id: self.id,
                               name: name,
                               student_number: student_number)
        unless edu_class_student.save
          error = email + ': ' + edu_class_student.errors.full_messages.join(' ')
        end
      else
        edu_class_student.name = name
        edu_class_student.student_number = student_number
        edu_class_student.save
      end
    end
    error
  end

  private
  def create_feedback_form
    FeedbackForm.create(edu_class_id: self.id)
  end

  def set_inbound_email_id
    require 'securerandom'
    self.inbound_email_id ||= SecureRandom.hex + 'c' + self.id.to_i.to_s(16) # need to convert to_i first
  end
end
