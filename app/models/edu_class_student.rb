class EduClassStudent < ApplicationRecord
  belongs_to :edu_class
  belongs_to :student
  has_many :chat_messages, dependent: :destroy

  validates :name, presence: true, length: { maximum: 200 }
  validates :student_number, presence: true, length: { maximum: 20 }
  validates_uniqueness_of :student, scope: :edu_class
end
