class Student < ApplicationRecord
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, password_length: 1..128

  has_many :edu_class_students
  has_many :edu_classes, through: :edu_class_students
  has_many :feedback_entries

  validates_uniqueness_of :email
  validates :name, presence: true, length: { maximum: 200 }
  validates :email, presence: true, length: { maximum: 200 }

end
