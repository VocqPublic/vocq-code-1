json.extract! message, :id, :created_at, :updated_at
json.type message.message_type
json.text message.content
