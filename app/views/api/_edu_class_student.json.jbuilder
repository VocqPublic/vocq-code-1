json.extract! edu_class_student, :id, :name, :created_at, :updated_at
json.student_id edu_class_student.student_number
json.email edu_class_student.student.email
