json.classId @selected_class_id
json.date @selected_date
json.classes @class_list, partial: 'api/edu_class', as: :edu_class 
json.dates @dates
json.records @feedback_entries
json.feedbacks_count @feedback_entries_count
json.students_count @students_count
json.questions @questions
json.timestamp @timestamp
