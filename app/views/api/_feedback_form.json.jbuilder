json.extract! feedback_form, :id, :title, :created_at, :updated_at
json.questions feedback_form.questions
json.colors feedback_form.colors
json.isPublished feedback_form.is_published
json.url feedback_form.url
json.questionTypes feedback_form.question_types
