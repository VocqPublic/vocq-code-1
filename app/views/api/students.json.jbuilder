json.classId @class_id
json.students @edu_class_students, partial: 'api/edu_class_student', as: :edu_class_student
json.classes @classes, partial: 'api/edu_class', as: :edu_class 
json.timestamp @timestamp
