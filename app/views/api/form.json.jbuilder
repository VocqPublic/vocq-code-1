json.classId @selected_class_id
json.classes @classes, partial: 'api/edu_class', as: :edu_class 
json.form @feedback_form, partial: 'api/feedback_form', as: :feedback_form
json.timestamp @timestamp
