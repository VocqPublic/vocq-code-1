json.date @selected_date
json.classId @selected_class_id
json.questions @questions
json.classes @class_list, partial: 'api/edu_class', as: :edu_class 
json.dates @dates
json.q1Data @q1Data
json.q2Data @q2Data
json.q3Data @q3Data
json.box1 @box1
json.box2 @box2
json.box3 @box3
json.box4 @box4
json.timestamp @timestamp
