json.students @students, partial: 'api/student', as: :student
json.classes @classes, partial: 'api/edu_class', as: :edu_class 
json.selected_class @selected_class
