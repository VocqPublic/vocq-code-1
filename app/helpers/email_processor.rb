class EmailProcessor
  def initialize(email)
    @email = email
  end

  def process
    inbound_email_id = @email.to[0][:token]
    student = Student.where(email: @email.from[:email]).first
    edu_class = EduClass.where(inbound_email_id: inbound_email_id).first
    unless student.nil? || edu_class.nil?
      edu_class_student = student.edu_class_students.where(edu_class_id: edu_class.id, student_id: student.id).first
    end

    unless edu_class_student.nil?
      content = @email.raw_text
      content.gsub!(/________________________________\nFrom:.*/m, '')
      content.gsub!(/On \d{1,2} \w+ \d{4} at \d{1,2}:\d{2}( [AP]M)?, .* <.*> wrote:.*/m, '')

      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_STUDENT,
                           content: content})
    end

    SendgridInboundDatum.create!(
      to: @email.to,
      from: @email.from,
      cc: @email.cc,
      subject: @email.subject,
      body: @email.body,
      raw_text: @email.raw_text,
      raw_html: @email.raw_html,
      raw_body: @email.raw_body,
      attachments: @email.attachments,
      headers: @email.headers,
      raw_headers: @email.raw_headers,
    )
  end
end
