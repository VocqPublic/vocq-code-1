module ApplicationHelper
  def self.send_email(from_email, from_name, to_email, to_name, subject, body, reply_to_email=nil)
    require 'sendgrid-ruby'
    include SendGrid
    from = Email.new(email: from_email, name: from_name)
    to = Email.new(email: to_email, name: to_name)
    content = Content.new(type: 'text/html', value: body)
    mail = Mail.new(from, subject, to, content)

    unless reply_to_email.nil?
      reply_to = Email.new(email: reply_to_email)
      mail.reply_to = reply_to
    end

    sg = SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'])
    response = sg.client.mail._('send').post(request_body: mail.to_json)
  end

end
