class FormEditorController < ApplicationController
  layout 'dashboard'
  before_action :authenticate_teacher!
  before_action :set_menu_title

  def index
    render file: 'public/dboard.html', layout: false
  end

  def update
    @feedback_form = current_user.schools.first.feedback_form
    name = params[:name]
    value = params[:value]

    respond_to do |format|
      @feedback_form[name] = value
      if @feedback_form.save
        format.html { render text: 'ok' }
        format.json { render :show, status: :ok, location: @feedback_form }
      else
        format.html { render text: 'error' }
        format.json { render json: @feedback_form.errors, status: :unprocessable_entity }
      end
    end
  end

  def set_menu_title
    @menu_title = 'Form Builder'
  end
end
