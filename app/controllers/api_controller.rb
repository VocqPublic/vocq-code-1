class ApiController < ApplicationController
  DATE_FORMAT = '%d-%m-%Y'
  SPA_TIMESTAMP = 1498006145

  before_action :set_default_response_format
  skip_before_action :verify_authenticity_token
  around_action :ensure_is_onboarded, only: [:dashboard, 
                                             :inbox,
                                             :form_builder] # maybe do except
  before_action :set_class_and_date, only: [:dashboard,
                                            :inbox,
                                            :students,
                                            :form,
                                            :form_replicate,
                                            :form_update_title,
                                            :form_update_question,
                                            :form_delete_question,
                                            :form_update_colors,
                                            :form_publish,
                                            :form_unpublish]
  before_action :set_timestamp

  def dashboard
    @students = @edu_class.students.order(:id).as_json

    # uniq dates
    @q1Data = []
    @dates.each do |d|
      d_obj = Date.strptime(d, DATE_FORMAT)
      
      score1_count = @edu_class.feedback_form.feedback_entries.where(date_start: d, question_score1: 1).count
      score2_count = @edu_class.feedback_form.feedback_entries.where(date_start: d, question_score1: 2).count
      score3_count = @edu_class.feedback_form.feedback_entries.where(date_start: d, question_score1: 3).count
      score4_count = @edu_class.feedback_form.feedback_entries.where(date_start: d, question_score1: 4).count
      score5_count = @edu_class.feedback_form.feedback_entries.where(date_start: d, question_score1: 5).count

      @q1Data.push({ day: d_obj.strftime('%d %b'), score1: score1_count, score2: score2_count, score3: score3_count, score4: score4_count, score5: score5_count })
    end

    @q2Data = []
    @q3Data = []
   
    feedback_form = @edu_class.feedback_form
    @questions = feedback_form.questions_list
    feedbacks_count = feedback_form.feedback_entries.where(date_start: @selected_date).count
    students_count = @students.count
    if feedbacks_count == 0
      @box1 = '0'
      @box2 = '0'
    else
      @box1 = feedback_form.feedback_entries.where(date_start: @selected_date).average(:question_score1).round(2).to_s
      @box2 = (feedbacks_count * 1.0 / students_count * 100).round().to_s
    end
    @box3 = feedbacks_count.to_s + '/' + students_count.to_s
    @box4 = @selected_date
  end

  def inbox
    @students = @edu_class.students.order(:id).as_json

    feedback_form = @edu_class.feedback_form
    @feedback_entries = feedback_form.feedback_entries.where(date_start: @selected_date).as_json

    @feedback_entries_count = @feedback_entries.count
    @students_count = @edu_class.students.count

    @questions = feedback_form.questions_list

    @student_feedback = []

    @students.each do |student|
      fb = @feedback_entries.find { |x| x['student_id'] == student['id'] }
      if fb.nil?
        @student_feedback.push({
          student_name: student['name'],
          student_id: student['id'],
          question1: 'N/A',
        })
      else
        q1 = feedback_form.question_type1 == FeedbackForm::QUESTION_TYPE_RATING ? fb['question_score1'] : fb['question1']
        q2 = feedback_form.question_type2 == FeedbackForm::QUESTION_TYPE_RATING ? fb['question_score2'] : fb['question2']
        q3 = feedback_form.question_type3 == FeedbackForm::QUESTION_TYPE_RATING ? fb['question_score3'] : fb['question3']
        q4 = feedback_form.question_type4 == FeedbackForm::QUESTION_TYPE_RATING ? fb['question_score4'] : fb['question4']
        q5 = feedback_form.question_type5 == FeedbackForm::QUESTION_TYPE_RATING ? fb['question_score5'] : fb['question5']

        @student_feedback.push({
          student_name: student['name'],
          student_id: student['id'],
          feedback_id: fb['id'],
          question1: q1,
          question2: q2,
          question3: q3,
          question4: q4,
          question5: q5,
        })
      end
    end

    @feedback_entries = @student_feedback
  end

  def messages
    edu_class_id = params[:class_id].to_i
    student_id = params[:student_id].to_i
    edu_class_student = EduClassStudent.where(edu_class_id: edu_class_id, student_id: student_id).first

    # make sure student's class belongs to teacher
    if edu_class_student.edu_class.teacher != current_teacher
      render json: []
    else
      @messages = edu_class_student.chat_messages.order(:created_at)
    end
  end

  def messages_new
    edu_class_id = params[:class_id].to_i
    student_id = params[:student_id].to_i
    edu_class_student = EduClassStudent.where(edu_class_id: edu_class_id, student_id: student_id).first

    # make sure student's class belongs to teacher
    if edu_class_student.edu_class.teacher != current_teacher
      render json: [], status: 401
    else
      ChatMessage.create({ edu_class_student_id: edu_class_student.id,
                           message_type: ChatMessage::MESSAGE_TYPE_TEACHER,
                           content: params[:message]}) # TODO: validate this
      @messages = edu_class_student.chat_messages.order(:created_at)
      render :messages
    end
  end

  def students
    @classes = current_teacher.edu_classes.order(:name)
    if @edu_class.nil? # if no class exist for teacher
      @edu_class_students = []
      @class_id = nil
    else
      @edu_class_students = @edu_class.edu_class_students.order(:id)
      @class_id = @edu_class.id
    end
  end

  def add_class
    edu_class = EduClass.new
    edu_class.name = params[:name]
    edu_class.teacher_id = current_teacher.id

    if edu_class.save
      @class_id = edu_class.id
      @classes = current_teacher.edu_classes.order(:name)
      @edu_class_students = edu_class.edu_class_students.order(:id)
      render :students
    else
      render json: { error: 'Error adding class.' }#, status: 403
    end
  end

  def delete_class
    current_teacher.edu_classes.where(id: params[:class_id]).destroy_all

    if current_teacher.edu_classes.count > 0
      edu_class = current_teacher.edu_classes.first
      @class_id = edu_class.id
      @classes = current_teacher.edu_classes.order(:name)
      @edu_class_students = edu_class.edu_class_students.order(:id)
    else
      @class_id = nil
      @classes = []
      @edu_class_students = []
    end

    render :students
  end

  def add_student
    @class_id = params[:class_id].to_i
    edu_class = EduClass.find(@class_id)
    edu_class.add_student(student_data[:email],
                          student_data[:name],
                          student_data[:student_id])
    render :students
  end

  def edit_student
  end

  def upload_student_list
    @class_id = params[:class_id].to_i
    edu_class = EduClass.find(@class_id)

    uploaded_io = params[:file]
    file_path = Rails.root.join('tmp', uploaded_io.original_filename)
    File.open(file_path, 'wb') do |file|
      file.write(uploaded_io.read)
    end

    file_csv_headers = File.open(file_path, &:readline).split(',')
    headers_list = ['student_id', 'name', 'email']
    new_header = []

    file_csv_headers.each_with_index {|file_header_val, file_header_index|
      headers_list.each_with_index {|header_val, header_index|
        if file_header_val.include? header_val
          new_header.push header_val
          break
        end
      }
    }

    if new_header.length == headers_list.length
      str = new_header.join(',')
      lines = File.readlines(file_path)
      lines[0] = str << $/
      File.open(file_path, 'w') { |f| f.write(lines.join) }
    else
      return
    end

    require 'smarter_csv'
    csv_data = SmarterCSV.process(file_path)
    
    errors = []
    csv_data.each do |student_data|
      err = edu_class.add_student(student_data[:email],
                                     student_data[:name],
                                     student_data[:student_id])
      unless err.nil?
        errors.push(err)
      end
    end

    @classes = current_teacher.edu_classes.order(:name)
    @edu_class_students = edu_class.edu_class_students

    if errors.length > 0
      render json: { errors: errors }
    else
      render :students
    end
  end

  def form
    @classes = current_teacher.edu_classes.order(:name)
    @feedback_form = @edu_class.feedback_form
  end

  def form_replicate
    @feedback_form = @edu_class.feedback_form
    @classes = current_teacher.edu_classes.order(:name)

    @classes.each do |c|
      unless c.feedback_form.id == @feedback_form.id
        c.feedback_form.replicate_form(@feedback_form)
      end
    end

    render :form
  end

  def form_update_title
    @feedback_form = @edu_class.feedback_form
    @feedback_form.title = params[:title]
    @feedback_form.save

    @classes = current_teacher.edu_classes.order(:name)
    render :form
  end

  def form_update_question
    @classes = current_teacher.edu_classes.order(:name)
    question_number = params[:number].to_i
    question_text = params[:text]
    question_type = params[:type]
    question_data = params[:data].as_json.to_json
    
    @feedback_form = @edu_class.feedback_form
    @feedback_form.update_question(question_number, question_text, question_type, question_data)

    @questions = @feedback_form.questions
    render :form
  end

  def form_delete_question
    @classes = current_teacher.edu_classes.order(:name)
    question_number = params[:number].to_i

    @feedback_form = @edu_class.feedback_form
    @feedback_form.delete_question(question_number)

    @questions = @feedback_form.questions

    render :form
  end

  def form_update_colors
    @feedback_form = @edu_class.feedback_form
    
    @feedback_form.color_background = params[:background]
    @feedback_form.color_foreground = params[:foreground]
    @feedback_form.color_question = params[:question]
    @feedback_form.color_help_text = params[:helpText]
    @feedback_form.color_button_background = params[:buttonBackground]
    @feedback_form.color_button_text = params[:buttonText]
    @feedback_form.save

    @classes = current_teacher.edu_classes.order(:name)

    render :form
  end

  def form_publish
    @feedback_form = @edu_class.feedback_form
    @feedback_form.publish
  end

  def form_unpublish
    @feedback_form = @edu_class.feedback_form
    @feedback_form.unpublish
    render :form_publish
  end

  def settings
    @name = current_teacher.name
    @email = current_teacher.email
    @designation = current_teacher.designation
  end

  def settings_update_info
    current_teacher.name = params[:name]
    current_teacher.email = params[:email]
    current_teacher.designation = params[:designation]
    if current_teacher.save
      @name = current_teacher.name
      @email = current_teacher.email
      @designation = current_teacher.designation
    else
      render json: { error: 1 }, status: 400 
    end
  end

  def settings_update_password
    unless current_teacher.valid_password?(params[:current_password])
      render json: { error: 'Wrong password' }, status: 400 
      return
    end

    current_teacher.password = params[:new_password]
    if current_teacher.save
      render json: { success: 1 }
    else
      render json: { error: 1 }, status: 400 
    end
  end

  protected
  def set_default_response_format
    request.format = :json
  end

  def set_class_and_date
    @edu_class = current_teacher.edu_classes.where(name: params[:class]).first

    if @edu_class.nil?
      @edu_class = current_teacher.edu_classes.first
    end

    # if class list is empty
    unless @edu_class.nil?
      @selected_class_id = @edu_class.id
    else
      @selected_class_id = -1
    end

    unless params[:date].blank?
      # handle invalid date param
      date = Date.strptime(params[:date], DATE_FORMAT).strftime(DATE_FORMAT) # not needed anymore, but leave this double conversion as date valudation for now
    end
    if date.blank?
      date = Time.zone.now.strftime(DATE_FORMAT)
    end

    @class_list = current_teacher.edu_classes.order(:name)

    # dates and date list now both the same
    unless @edu_class.nil?
      @dates = @edu_class.feedback_form.feedback_entries.select(:date_start).distinct(:date_start).order(:date_start).pluck(:date_start)

      # if date dont exist, set to latest
      unless @dates.include? date
        date = @dates.last
      end

      @selected_date = date
    end
  end

  def ensure_is_onboarded
    onboard_status = current_teacher.latest_onboard_status
    if onboard_status == Teacher::ONBOARD_STATUS_OK
      yield
    else
      error_message = current_teacher.onboard_error_message
      render json: { error: error_message }
    end
  end

  def set_timestamp
    #@timestamp = Time.zone.now.to_i
    @timestamp = SPA_TIMESTAMP
  end
end
