class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  layout :layout_by_resource

  skip_before_action :verify_authenticity_token, if: :devise_controller?

  def after_sign_in_path_for(resource_or_scope)
    if resource_or_scope.is_a?(Teacher)
      dashboard_path
    elsif resource_or_scope.is_a?(Student)
      student_portal_path
    else
      root_path      
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    if resource_or_scope == :teacher
      new_teacher_path
    elsif resource_or_scope == :student
      student_portal_path
    else
      root_path      
    end
  end

  def layout_by_resource
    if devise_controller?
      if resource_name == :student
        'students'
      else
        'devise'
      end
    else
      'application'
    end
  end

  private

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end
end
