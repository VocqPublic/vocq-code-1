class DashboardController < ApplicationController
  before_action :authenticate_teacher!

  def index
    render file: 'public/dboard.html', layout: false
  end
end

