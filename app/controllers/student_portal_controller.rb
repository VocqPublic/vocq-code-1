class StudentPortalController < ApplicationController
  layout 'students'
  before_action :authenticate_student!

  def index
    classes
  end

  def classes
    edu_classes = current_student.edu_classes.all
    @class_list = []
    edu_classes.each do |c|
      form = c.feedback_form
      if form.is_published
        if current_student.feedback_entries.where(feedback_form_id: form.id, date_start: Time.zone.now.strftime(FeedbackEntry::DATE_START_FORMAT)).count == 0
          @class_list.push(id: c.id, name: c.feedback_form.title)
        end
      end
    end
  end

  def subject
    edu_class = current_student.edu_classes.where(id: params[:id]).first
    unless edu_class.nil?
      @form = edu_class.feedback_form
    end
  end

  def submit
    feedback_entry = FeedbackEntry.new(student_id: current_student.id,
                     feedback_form_id: params[:form_id])

    unless params[:question1].blank?
      feedback_entry.question1 = params[:question1]
      feedback_entry.question_score1 = params[:question1].to_i
    end

    unless params[:question2].blank?
      feedback_entry.question2 = params[:question2]
      feedback_entry.question_score2 = params[:question2].to_i
    end

    unless params[:question3].blank?
      feedback_entry.question3 = params[:question3]
      feedback_entry.question_score3 = params[:question3].to_i
    end
    unless params[:question4].blank?
      feedback_entry.question4 = params[:question4]
      feedback_entry.question_score4 = params[:question4].to_i
    end
    unless params[:question5].blank?
      feedback_entry.question5 = params[:question5]
      feedback_entry.question_score5 = params[:question5].to_i
    end

    if feedback_entry.save
      render json: { success: true }
    else
      render json: { success: false, error: feedback_entry.errors.full_messages.join('. ') }
    end

  end
end

