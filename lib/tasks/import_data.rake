namespace :import_data do
  desc "TODO"
  task coursemirror: :environment do
    require 'csv'

    user = User.where(email: 'nygh@vocq.co').first
    feedback_form = user.schools.first.feedback_form

    (1..12).each do |i|
      input_file = "data/#{i}.csv"
      feedback_data = File.read(input_file)
      feedback_data = CSV.new(feedback_data)

      date = Date.today.weeks_ago(15-i)

      feedback_data.each do |fb|
        student = Student.find_or_create_by(student_id: fb[0], school_id: feedback_form.school_id)
        fbe = FeedbackEntry.create(score: 1 + rand(5),
                    question2: fb[1].blank? ? 'None' : fb[1],
                    question3: fb[2].blank? ? 'None' : fb[2],
                    student_id: student.id,
                    feedback_form_id: feedback_form.id,
                    created_at: date,
                    updated_at: date)
        puts fbe.errors.full_messages
      end
    end
  end


  task coursemirror_tagged: :environment do
    require 'csv'

    user = User.where(email: 'james@vocq.co').first
    feedback_form = user.schools.first.feedback_form

    (1..2).each do |i|
      input_file = "data/data_tagged/#{i}.csv"
      feedback_data = File.read(input_file)
      feedback_data = CSV.new(feedback_data)

      date = Date.today.weeks_ago(15-i)

      feedback_data.each do |fb|
        student = Student.find_or_create_by(student_id: fb[0], school_id: feedback_form.school_id)
        fbe = FeedbackEntry.create(score: 1 + rand(5),
                    question2: fb[1].blank? ? 'None' : fb[1],
                    question2_topic: fb[2].blank? ? 'None' : fb[2],
                    question3: fb[3].blank? ? 'None' : fb[3],
                    question3_topic: fb[4].blank? ? 'None' : fb[4],
                    student_id: student.id,
                    feedback_form_id: feedback_form.id,
                    week: 'Y' + date.year.to_s + 'W' + (date.strftime('%U').to_i + 1).to_s,
                    created_at: date,
                    updated_at: date)
        puts fbe.errors.full_messages
      end
    end
  end



  task four_classes: :environment do
    teacher = Teacher.where(email: 'james@vocq.co').first
    require 'smarter_csv'

    Dir.glob('data/four_classes/students/*.csv') do |file_path|
      csv_data = SmarterCSV.process(file_path)
      class_name = File.basename(file_path, ".csv")
      edu_class = EduClass.find_or_create_by(teacher_id: teacher.id, name: class_name, subject: 'English')

      csv_data.each do |student|
        #student[:teacher_id] = teacher.id
        student[:password] = student[:student_id]
        student = Student.new(student)
        Student.transaction do
          if student.save
            EduClassStudent.create(student_id: student.id, edu_class_id: edu_class.id)
          end
        end
      end
    end

    Dir.glob('data/four_classes/feedback/*.csv') do |file_path|
      csv_data = SmarterCSV.process(file_path)
      class_name = File.basename(file_path, ".csv")
      edu_class = EduClass.find_or_create_by(teacher_id: teacher.id, name: class_name)
      form = edu_class.feedback_form

      csv_data.each do |feedback|
        feedback[:feedback_form_id] = form.id
        feedback[:student_id] = Student.where(student_id: feedback[:student_id]).first.id
        feedback[:created_at] = Date.strptime(feedback[:created_at], "%m/%d/%Y")
        feedback[:date_start] = feedback[:created_at].strftime(FeedbackEntry::DATE_START_FORMAT)
        feedback_entry = FeedbackEntry.create(feedback)
      end
    end
  end








  task demo_account: :environment do
    teacher = Teacher.where(email: 'demo@vocq.co').first
    require 'smarter_csv'

    Dir.glob('data/four_classes/students/*.csv') do |file_path|
      csv_data = SmarterCSV.process(file_path)
      class_name = File.basename(file_path, ".csv")
      edu_class = EduClass.find_or_create_by(teacher_id: teacher.id, name: class_name, subject: 'English')

      csv_data.each do |student|
        #student[:teacher_id] = teacher.id
        student[:password] = student[:student_id]
        student = Student.new(student)
        Student.transaction do
          if student.save
            EduClassStudent.create(student_id: student.id, edu_class_id: edu_class.id)
          end
        end
      end
    end

    Dir.glob('data/four_classes/feedback/*.csv') do |file_path|
      csv_data = SmarterCSV.process(file_path)
      class_name = File.basename(file_path, ".csv")
      edu_class = EduClass.find_or_create_by(teacher_id: teacher.id, name: class_name)
      form = edu_class.feedback_form

      csv_data.each do |feedback|
        feedback[:feedback_form_id] = form.id
        feedback[:student_id] = Student.where(student_id: feedback[:student_id]).first.id
        feedback[:created_at] = Date.strptime(feedback[:created_at], "%m/%d/%Y")
        feedback[:date_start] = feedback[:created_at].strftime(FeedbackEntry::DATE_START_FORMAT)
        feedback_entry = FeedbackEntry.create(feedback)
      end
    end
  end









end
