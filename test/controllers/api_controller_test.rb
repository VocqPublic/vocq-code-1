require 'test_helper'

class ApiControllerTest < ActionDispatch::IntegrationTest
  test "should get inbox" do
    get api_inbox_url
    assert_response :success
  end

  test "should get student" do
    get api_student_url
    assert_response :success
  end

  test "should get dashboard" do
    get api_dashboard_url
    assert_response :success
  end

end
