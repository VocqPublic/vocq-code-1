Rails.application.routes.draw do
  default_url_options host: ENV['HOST']

  devise_for :teachers
  devise_for :students

  mount_griddler('inbound/sendgrid')

  root to: "dashboard#index"

  get 'dashboard', to: 'dashboard#index', as: 'dashboard'
  get 'inbox', to: 'dashboard#index'
  get 'class', to: 'dashboard#index'
  get 'form', to: 'dashboard#index'
  get 'settings', to: 'dashboard#index'


  get 'api/dashboard'
  get 'api/inbox'

  get 'api/classes/:class_id/students/:student_id/messages', to: 'api#messages'
  post 'api/classes/:class_id/students/:student_id/messages', to: 'api#messages_new'

  get 'api/settings', to: 'api#settings'
  put 'api/settings/info', to: 'api#settings_update_info'
  put 'api/settings/password', to: 'api#settings_update_password'

  
  # Class Page
  get 'api/class', to: 'api#students'
  post 'api/class', to: 'api#add_class'
  delete 'api/class/:class_id', to: 'api#delete_class'
  post 'api/class/upload_list', to: 'api#upload_student_list'

  # Form Page
  get 'api/form',                     to: 'api#form'
  put 'api/form/title',               to: 'api#form_update_title'
  put 'api/form/question',            to: 'api#form_update_question' # change to patch
  put 'api/form/colors',              to: 'api#form_update_colors'
  delete 'api/form/question/:number', to: 'api#form_delete_question'
  post 'api/form/publish',            to: 'api#form_publish'
  post 'api/form/unpublish',          to: 'api#form_unpublish'
  post 'api/form/replicate',          to: 'api#form_replicate'

  get 'student-portal', to: 'student_portal#index', as: 'student_portal'
  get 'student-portal/classes', to: 'student_portal#classes', as: 'student_portal_classes'
  get 'student-portal/subject', to: 'student_portal#subject', as: 'student_portal_subject'
  post 'student-portal/submit', to: 'student_portal#submit', as: 'student_portal_submit'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
